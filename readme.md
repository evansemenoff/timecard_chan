# Timesheet-Chan
Punchcard-Chan is a discord bot that automates clock-in/clock-out times for employees and integrates with Google Sheets to automate the entire timecard process.  Simply add the bot to your company discord server, set up the the Google Sheet with the correct date ranges and formatting, then add your employee names to the *discord_usernames.py* file (in Python dictionary syntax, example included). 

## Setup
### Local
In **settings.py**, you must enter the timesheet name as the variable timesheet_name, as well as the name you want for the log file in the variable log_file.  The server_name variable is optional and simply for aesthetics.  Finally, you have to give the name of the timecard channel on the server.

You will need to create a new Google Sheets project and generate the oAuth token to be stored locally under the name "timesheet_chan_creds.json".  Instructions on how to generate the token are found here https://developers.google.com/sheets/api/guides/authorizing.

### Server
The only thing to do on the discord server side is to add the bot to the discord server, and make sure that a channel exists with the same name that is set in the **settings.py** file.

### Sheets
You will need a Google sheet with dates ranging from B8:B21 (two weeks).  Later this may be generalized in the **settings.py** file.

For each employee, they must have their *sheet name* and *Discord username* added to the dictionary in **discord_usernames.py**, where the key is the Discord username *without* the discriminator (eg #0042), and the value is the *sheet name* corresponding to that user's timesheet.
