class Datecache:
    """Added to the project to reduce the amount of times the service needs to ping Sheets for date ranges.
    New cache is only generated on a cache miss."""
    def __init__(self, worksheet):
        self.date_dict = {}
        self.worksheet = worksheet

    def generate_cache(self):
        day_cells = ["B" + str(i) for i in range(8, 22)]
        for i in day_cells:
            self.date_dict[self.worksheet.acell(i).value] = i.split("B")[1]

    def get_cell(self, date):
        if date in self.date_dict:
            return self.date_dict[date]
        else:
            self.generate_cache()
            return self.date_dict[date]

