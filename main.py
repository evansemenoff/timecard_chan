import discord
import gspread
from datecache import Datecache
from datetime import datetime
from lunch import Lunch
from discord_usernames import username_map
from settings import *
from logger import Logger

client = discord.Client()


def init():
    global logger, main_sheet, datecache, lunch_map
    logger = Logger(log_file)
    logger.print_and_log(" Building lunch cache")
    lunch_map = {}
    for user in username_map.keys():
        lunch_map[username_map[user]] = Lunch()
    logger.print_and_log(" Connecting to sheets")
    g_service_acct = gspread.service_account(filename="timesheet_chan_creds.json")
    main_sheet = g_service_acct.open(timesheet_name)
    logger.print_and_log(" Building date cache")
    datecache = Datecache(main_sheet.worksheet("Evan"))
    datecache.generate_cache()
    logger.print_and_log(" Setup done")


@client.event
async def on_message(message):
    """Entry point for the important work. Tokenizes the message, creates timestamps and dispatches commands.
    Dispatch is done here for ease of async management"""
    logger.log(str(locals()))
    if message.author == client.user or str(message.channel) != timecard_channel:
        return

    current_time = datetime.now().strftime("%I:%M %p")
    current_user = username_map[str(message.author).split("#")[0]]

    message_tokens = message.content.lower().split(" ")

    if message_tokens[0] == "!help":
        await message.channel.send(help_command(message_tokens))

    elif message_tokens[0] == "!clock":
        await message.channel.send(clock_command(message_tokens,
                                                 current_time,
                                                 current_user))

    elif message_tokens[0] == "!lunch":
        await message.channel.send(lunch_command(message_tokens,
                                                 current_time,
                                                 current_user))


def help_command(message_tokens):
    command = " ".join(message_tokens[1::])
    match command:
        case "!clock in":
            return "Usage: [!clock in] or [!clock in <TIME>] where <TIME> is a military timestamp. If " \
                   "given no second argument, [!clock in] will default to use the current time."
        case "!clock out":
            return "Usage: [!clock out] or [!clock out <TIME>] where <TIME> is a valid timestamp. " \
                   "If given no second argument, [!clock out] will default to use the current time."
        case "!help":
            return "Usage: [!help <COMMAND>].  Displays command usage."
        case "!lunch":
            return "Usage: [!lunch start/end] or [!lunch <DURATION>].  Starts lunch timer with [!lunch start]" \
                   " and ends and logs time with [!lunch end].  If  given a duration in the format of [!lunch HH:MM]" \
                   " this value will be logged as the duration for lunch."
        case _:
            return "Available commands are [!help], [!clock in], [!clock out], and [!lunch].  Type !help <COMMAND>" \
                   " for more info about that particular command."


def clock_command(message_tokens, current_time, current_user):
    logger.log(str(locals()))
    current_day = datetime.now().strftime("%d %B")
    # for user inputting their start/end time directly
    if len(message_tokens) == 3:
        current_time = message_tokens[2]
    if message_tokens[1] == "in":
        log_time(current_user, current_time, "C")
        return f"Okay {current_user}, I clocked you in for {current_day} at: {current_time}."
    elif message_tokens[1] == "out":
        log_time(current_user, current_time, "D")
        return f"Okay {current_user}, I clocked you out for {current_day} at {current_time}."
    return "Malformed clock in/out message."


def lunch_command(message_tokens, current_time, current_user):
    if message_tokens[1] == "start":
        lunch_map[current_user.capitalize()].start_lunch(datetime.now())
        return f'Okay {current_user}, I logged your lunch start time at {current_time}'
    elif message_tokens[1] == "end":
        lunch_map[current_user.capitalize()].end_lunch(datetime.now())
        lunch_length = lunch_map[current_user.capitalize()].get_lunch_length()
        log_time(current_user, str(lunch_length[0]) + ":" + str(lunch_length[1]).zfill(2), "E")
        time_taken = str(lunch_length[0]) + " hours and " + str(lunch_length[1]) + " minutes"
        return f'Okay {current_user}, I logged your lunch end time at {current_time} ' \
               f'with a total of {time_taken} taken for lunch.'
    elif message_tokens[1]:
        lunch_length = message_tokens[1]
        log_time(current_user, lunch_length, "E")
        return f'Okay {current_user}, I logged the amount of time you took for lunch as {lunch_length}.'
    return "Malformed lunch command."


def log_time(username, current_time, column_name):
    wks = main_sheet.worksheet(username)
    date_string = datetime.now().strftime("%B %#d")
    row_name = datecache.get_cell(date_string)
    date_cell = column_name + row_name
    wks.update(date_cell, current_time)


def main():
    init()
    token = open("discord_token.txt", "r").readline()
    client.run(token)


main()
