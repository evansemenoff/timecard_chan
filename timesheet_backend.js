function FullRollOver(){
  SavePeriod();
  workers = GetWorkers();
  for (var counter = 0; counter <= workers.length + 1; counter = counter + 1){
    Rollover('B8:B21',workers[0][counter]);
    Rollover('H8:H21',workers[0][counter]);
  }
  ClearCells();
}

function ClearCells() {
  // clears all of the old time sheets & transfers new timesheets over
  workers = GetWorkers();
  for (var counter = 0; counter <= workers.length + 1; counter = counter + 1) {
    var worker = workers[0][counter];
    console.log(worker, counter)
    var sheet = SpreadsheetApp.getActive().getSheetByName(worker);

    // clear old period
    sheet.getRange('C8:C21').clearContent();
    sheet.getRange('D8:D21').clearContent();
    sheet.getRange('E8:E21').clearContent();

    //move "next period" to "current period"
    sheet.getRange('C8:C21').setValues(sheet.getRange('I8:I21').getValues());
    sheet.getRange('D8:C21').setValues(sheet.getRange('J8:I21').getValues());
    sheet.getRange('E8:C21').setValues(sheet.getRange('K8:I21').getValues());

    //clear "next period"
    sheet.getRange('I8:I21').clearContent();
    sheet.getRange('J8:J21').clearContent();
    sheet.getRange('K8:K21').clearContent();

  }
}

function GetWorkers(){
  var sheet = SpreadsheetApp.getActive().getSheetByName("Master");
  var names = sheet.getRange('B7:E7').getValues();
  console.log(names)
  return names
}

function Add14(date){
  // needed for rollover, responsible for adding 2 weeks
  var date = new Date(date);
  date.setDate(date.getDate() + 14);
  return [date];
}

function Rollover(range, worker){
  // rolls over all of the dates on the sheet to the new period
  var sheet = SpreadsheetApp.getActive().getSheetByName(worker);
  var dates = sheet.getRange(range).getValues();
  newdates = dates.map(x => Add14(x));
  console.log(newdates);
  sheet.getRange(range).setValues(newdates);
}

function SavePeriod(){
  // saves the backup sheet for the current period
  var sheet = SpreadsheetApp.getActive().getSheetByName("Master");
  var start = new Date(sheet.getRange("C3").getValue());
  var startVal = start.getMonth() + "-" + start.getDate();
  var end = new Date(sheet.getRange("C4").getValue());
  var endVal = end.getMonth() + "-" + end.getDate();
  var destFolder = DriveApp.getFolderById("19s5NxYjazCbxw51RgokUx8c7MoM4Fnfj");
  DriveApp.getFileById("1g8RlKuIOGAYvrQGYDmVfrqoQDhJ0nDHHyHMvwZAZJ-8")
  .makeCopy("Timesheet " + startVal + " to " + endVal, destFolder);
}





