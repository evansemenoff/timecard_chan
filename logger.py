import datetime


class Logger:
    """Quick logger class to log events and output during initialization"""

    def __init__(self, filename):
        self.local_file = open(filename, "a")
        self.write_string = ""

    def generate_write_string(self, *args):
        event_info = " ".join(args)
        curr_year = datetime.datetime.now().strftime("%Y")
        curr_date = datetime.datetime.now().strftime("%d %B")
        curr_time = datetime.datetime.now().strftime("%I:%M %p")
        self.write_string = "[" + str(curr_year) + " " \
                       + str(curr_date) + " " \
                       + str(curr_time) + "]" \
                       + event_info + "\n"

    def log(self, *args):
        self.generate_write_string(*args)
        self.local_file.write(self.write_string)
        self.local_file.flush()

    def print_and_log(self, *args):
        self.generate_write_string(*args)
        self.local_file.write(self.write_string)
        print(self.write_string)
        self.local_file.flush()
