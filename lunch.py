class Lunch:
    """Stores timestamps for starting and ending lunch and stores the logic for
    how to translate those timestamps."""

    def __init__(self):
        self.start_time = None
        self.end_time = None
        self.total_lunch_time = None

    def start_lunch(self, start_time):
        self.start_time = start_time

    def end_lunch(self, end_time):
        self.end_time = end_time
        self.total_lunch_time = self.end_time - self.start_time

    def get_lunch_length(self):
        return self.total_lunch_time.seconds // 3600, \
               self.total_lunch_time.seconds // 60 % 60

            self.total_lunch_time.seconds // 60 % 60

