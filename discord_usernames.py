# this file is responsible for the mapping from discord username -> google sheets name
# if you add a new employee, you will need to add them to this file in the form of
# "<DISCORD_USERNAME>": "<GOOGLE_SHEETS_TAB_NAME>"
# NOTE: you do NOT need to add the username discriminator (the part after #)

username_map = {"SmoothManSam": "Ben",
                "evan": "Evan",
                "mega9f7": "Nathan",
                "Pole": "Aiden",
                "Dwilly": "David"}
